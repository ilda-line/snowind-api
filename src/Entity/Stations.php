<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource(
 *     collectionOperations={"get"},
 *     itemOperations={"get"}
 * )
 * @ORM\Entity(repositoryClass="App\Repository\StationsRepository")
 */
class Stations
{
    /**
     * @Groups("ressource")
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Groups("ressource")
     * @ORM\Column(type="string", length=255)
     */
    private $NomStation;

    /**
     * @Groups("ressource")
     * @ORM\Column(type="boolean")
     */
    private $EtatStation;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\MeteoStation", mappedBy="station")
     */
    private $meteo;

    /**
     * @ORM\Column(type="integer")
     */
    private $id_API;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Voie", mappedBy="Station")
     */
    private $voies;

    /**
     * @Groups("ressource")
     * @ORM\Column(type="string", length=255)
     */
    private $localisation;

    /**
     * @ORM\Column(type="integer")
     */
    private $altitude_bas;

    /**
     * @Groups("ressource")
     */
    private $altitudeBas;

    /**
     * @ORM\Column(type="integer")
     */
    private $altitude_haut;

    /**
     * @Groups("ressource")
     */
    private $altitudeHaut;

    public function __construct()
    {
        $this->meteo = new ArrayCollection();
        $this->voies = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomStation(): ?string
    {
        return $this->NomStation;
    }

    public function setNomStation(string $NomStation): self
    {
        $this->NomStation = $NomStation;

        return $this;
    }

    public function getEtatStation(): ?bool
    {
        return $this->EtatStation;
    }

    public function setEtatStation(bool $EtatStation): self
    {
        $this->EtatStation = $EtatStation;

        return $this;
    }

    /**
     * @return Collection|MeteoStation[]
     */
    public function getMeteo(): Collection
    {
        return $this->meteo;
    }

    public function addMeteo(MeteoStation $meteo): self
    {
        if (!$this->meteo->contains($meteo)) {
            $this->meteo[] = $meteo;
            $meteo->setStation($this);
        }

        return $this;
    }

    public function removeMeteo(MeteoStation $meteo): self
    {
        if ($this->meteo->contains($meteo)) {
            $this->meteo->removeElement($meteo);
            // set the owning side to null (unless already changed)
            if ($meteo->getStation() === $this) {
                $meteo->setStation(null);
            }
        }

        return $this;
    }

    public function getIdAPI(): ?int
    {
        return $this->id_API;
    }

    public function setIdAPI(int $id_API): self
    {
        $this->id_API = $id_API;

        return $this;
    }

    /**
     * @return Collection|Voie[]
     */
    public function getVoies(): Collection
    {
        return $this->voies;
    }

    public function addVoie(Voie $voie): self
    {
        if (!$this->voies->contains($voie)) {
            $this->voies[] = $voie;
            $voie->setStation($this);
        }

        return $this;
    }

    public function removeVoie(Voie $voie): self
    {
        if ($this->voies->contains($voie)) {
            $this->voies->removeElement($voie);
            // set the owning side to null (unless already changed)
            if ($voie->getStation() === $this) {
                $voie->setStation(null);
            }
        }

        return $this;
    }

    public function getLocalisation(): ?string
    {
        return $this->localisation;
    }

    public function setLocalisation(string $localisation): self
    {
        $this->localisation = $localisation;

        return $this;
    }

    public function getAltitudeBas(): ?int
    {
        return $this->altitude_bas;
    }

    public function setAltitudeBas(int $altitude_bas): self
    {
        $this->altitude_bas = $altitude_bas;

        return $this;
    }

    public function getAltitudeHaut(): ?int
    {
        return $this->altitude_haut;
    }

    public function setAltitudeHaut(int $altitude_haut): self
    {
        $this->altitude_haut = $altitude_haut;

        return $this;
    }
}
