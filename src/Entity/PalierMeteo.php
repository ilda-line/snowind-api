<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource(
 *     collectionOperations={"get"},
 *     itemOperations={"get"}
 * )
 * @ORM\Entity(repositoryClass="App\Repository\PalierMeteoRepository")
 */
class PalierMeteo
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Groups("ressource")
     * @ORM\Column(type="string", length=255)
     */
    private $type;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\MeteoSpecifique", mappedBy="palier_meteo")
     */
    private $meteoSpecifiques;

    /**
     * @Groups("ressource")
     * @ORM\ManyToMany(targetEntity="App\Entity\Voie", mappedBy="liaison_meteo")
     */
    private $voies;

    public function __construct()
    {
        $this->meteoSpecifiques = new ArrayCollection();
        $this->voies = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return Collection|MeteoSpecifique[]
     */
    public function getMeteoSpecifiques(): Collection
    {
        return $this->meteoSpecifiques;
    }

    public function addMeteoSpecifique(MeteoSpecifique $meteoSpecifique): self
    {
        if (!$this->meteoSpecifiques->contains($meteoSpecifique)) {
            $this->meteoSpecifiques[] = $meteoSpecifique;
            $meteoSpecifique->setPalierMeteo($this);
        }

        return $this;
    }

    public function removeMeteoSpecifique(MeteoSpecifique $meteoSpecifique): self
    {
        if ($this->meteoSpecifiques->contains($meteoSpecifique)) {
            $this->meteoSpecifiques->removeElement($meteoSpecifique);
            // set the owning side to null (unless already changed)
            if ($meteoSpecifique->getPalierMeteo() === $this) {
                $meteoSpecifique->setPalierMeteo(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Voie[]
     */
    public function getVoies(): Collection
    {
        return $this->voies;
    }

    public function addVoie(Voie $voie): self
    {
        if (!$this->voies->contains($voie)) {
            $this->voies[] = $voie;
            $voie->addLiaisonMeteo($this);
        }

        return $this;
    }

    public function removeVoie(Voie $voie): self
    {
        if ($this->voies->contains($voie)) {
            $this->voies->removeElement($voie);
            $voie->removeLiaisonMeteo($this);
        }

        return $this;
    }
}
