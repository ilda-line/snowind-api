<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource(
 *     collectionOperations={"get"},
 *     itemOperations={"get"}
 * )
 * @ORM\Entity(repositoryClass="App\Repository\VoieRepository")
 */
class Voie
{
    /**
     * @Groups("ressource")
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Groups("ressource")
     * @ORM\Column(type="string", length=255)
     */
    private $Nom;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Etat;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $frequentation;

    /**
     * @Groups("ressource")
     * @ORM\ManyToOne(targetEntity="App\Entity\CategoryVoie", inversedBy="voie", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $categorie;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Stations", inversedBy="voies")
     * @ORM\JoinColumn(nullable=false)
     */
    private $Station;

    /**
     * @Groups("ressource")
     * @ORM\ManyToOne(targetEntity="App\Entity\PalierRefPiste", inversedBy="voies")
     * @ORM\JoinColumn(nullable=false)
     */
    private $palierRef;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\PalierMeteo", inversedBy="voies")
     */
    private $liaison_meteo;


    public function __construct()
    {
        $this->liaison_meteo = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->Nom;
    }

    public function setNom(string $Nom): self
    {
        $this->Nom = $Nom;

        return $this;
    }

    public function getEtat(): ?string
    {
        return $this->Etat;
    }

    public function setEtat(string $Etat): self
    {
        $this->Etat = $Etat;

        return $this;
    }


    public function getFrequentation(): ?int
    {
        return $this->frequentation;
    }

    public function setFrequentation(?int $frequentation): self
    {
        $this->frequentation = $frequentation;

        return $this;
    }

    public function getCategorie(): ?CategoryVoie
    {
        return $this->categorie;
    }

    public function setCategorie(?CategoryVoie $categorie): self
    {
        $this->categorie = $categorie;

        return $this;
    }

    public function getStation(): ?Stations
    {
        return $this->Station;
    }

    public function setStation(?Stations $Station): self
    {
        $this->Station = $Station;

        return $this;
    }

    public function getPalierRef(): ?PalierRefPiste
    {
        return $this->palierRef;
    }

    public function setPalierRef(?PalierRefPiste $palierRef): self
    {
        $this->palierRef = $palierRef;

        return $this;
    }

    /**
     * @return Collection|PalierMeteo[]
     */
    public function getLiaisonMeteo(): Collection
    {
        return $this->liaison_meteo;
    }

    public function addLiaisonMeteo(PalierMeteo $liaisonMeteo): self
    {
        if (!$this->liaison_meteo->contains($liaisonMeteo)) {
            $this->liaison_meteo[] = $liaisonMeteo;
        }

        return $this;
    }

    public function removeLiaisonMeteo(PalierMeteo $liaisonMeteo): self
    {
        if ($this->liaison_meteo->contains($liaisonMeteo)) {
            $this->liaison_meteo->removeElement($liaisonMeteo);
        }

        return $this;
    }


}
