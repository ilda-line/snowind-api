<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource(
 *     collectionOperations={"get"},
 *     itemOperations={"get"}
 * )
 * @ORM\Entity(repositoryClass="App\Repository\IndiceSkiabiliteRepository")
 */
class IndiceSkiabilite
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     * @Groups("ressource")
     */
    private $indiceSkiabilite;

    /**
     * @Groups("ressource")
     * @ORM\ManyToOne(targetEntity="App\Entity\PalierRefPiste", inversedBy="indiceSkiabilites")
     * @ORM\JoinColumn(nullable=false)
     */
    private $PalierRef;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\MeteoStation", inversedBy="indiceSkiabilites")
     * @ORM\JoinColumn(nullable=false)
     */
    private $meteo_commune;

    public function __construct()
    {

    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIndiceSkiabilite(): ?int
    {
        return $this->indiceSkiabilite;
    }

    public function setIndiceSkiabilite(int $indiceSkiabilite): self
    {
        $this->indiceSkiabilite = $indiceSkiabilite;

        return $this;
    }

    public function getPalierRef(): ?PalierRefPiste
    {
        return $this->PalierRef;
    }

    public function setPalierRef(?PalierRefPiste $PalierRef): self
    {
        $this->PalierRef = $PalierRef;

        return $this;
    }

    public function getMeteoCommune(): ?MeteoStation
    {
        return $this->meteo_commune;
    }

    public function setMeteoCommune(?MeteoStation $meteo_commune): self
    {
        $this->meteo_commune = $meteo_commune;

        return $this;
    }
}
