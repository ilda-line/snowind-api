<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource(
 *     collectionOperations={"get"},
 *     itemOperations={"get"}
 * )
 * @ORM\Entity(repositoryClass="App\Repository\CategoryVoieRepository")
 */
class CategoryVoie
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Groups("ressource")
     * @ORM\Column(type="string", length=255)
     */
    private $type;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Voie", mappedBy="categorie")
     */
    private $voie;

    public function __construct()
    {
        $this->voie = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return Collection|Voie[]
     */
    public function getVoie(): Collection
    {
        return $this->voie;
    }

    public function addVoie(Voie $voie): self
    {
        if (!$this->voie->contains($voie)) {
            $this->voie[] = $voie;
            $voie->setCategorie($this);
        }

        return $this;
    }

    public function removeVoie(Voie $voie): self
    {
        if ($this->voie->contains($voie)) {
            $this->voie->removeElement($voie);
            // set the owning side to null (unless already changed)
            if ($voie->getCategorie() === $this) {
                $voie->setCategorie(null);
            }
        }

        return $this;
    }
}
