<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource(
 *     collectionOperations={"get"},
 *     itemOperations={"get"}
 * )
 * @ORM\Entity(repositoryClass="App\Repository\RefMeteoRepository")
 */
class RefMeteo
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Groups("ressource")
     * @ORM\Column(type="string", length=255)
     */
    private $description;

    /**
     * @ORM\Column(type="integer")
     */
    private $points_indice;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\MeteoSpecifique", mappedBy="Description_temps")
     */
    private $meteoSpecifiques;

    public function __construct()
    {
        $this->meteoSpecifiques = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getPointsIndice(): ?int
    {
        return $this->points_indice;
    }

    public function setPointsIndice(int $points_indice): self
    {
        $this->points_indice = $points_indice;

        return $this;
    }

    /**
     * @return Collection|MeteoSpecifique[]
     */
    public function getMeteoSpecifiques(): Collection
    {
        return $this->meteoSpecifiques;
    }

    public function addMeteoSpecifique(MeteoSpecifique $meteoSpecifique): self
    {
        if (!$this->meteoSpecifiques->contains($meteoSpecifique)) {
            $this->meteoSpecifiques[] = $meteoSpecifique;
            $meteoSpecifique->setDescriptionTemps($this);
        }

        return $this;
    }

    public function removeMeteoSpecifique(MeteoSpecifique $meteoSpecifique): self
    {
        if ($this->meteoSpecifiques->contains($meteoSpecifique)) {
            $this->meteoSpecifiques->removeElement($meteoSpecifique);
            // set the owning side to null (unless already changed)
            if ($meteoSpecifique->getDescriptionTemps() === $this) {
                $meteoSpecifique->setDescriptionTemps(null);
            }
        }

        return $this;
    }
}
