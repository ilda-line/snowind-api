<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Doctrine\ORM\Mapping as ORM;

/**

 * @ApiResource(
 *     collectionOperations={"get"},
 *     itemOperations={"get"}
 * )
 * @ApiFilter(SearchFilter::class, properties={"meteo_commune": "exact"})
 * @ORM\Entity(repositoryClass="App\Repository\MeteoSpecifiqueRepository")
 */
class MeteoSpecifique
{
    /**
     * @Groups("ressource")
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;


    /**
     * @ORM\Column(type="integer")
     */
    private $neige_fraiche_cm;

    /**
     * @Groups("ressource")
     */
    private $neigeFraicheCm;

    /**
     * @Groups("ressource")
     * @ORM\Column(type="float", nullable=true)
     */
    private $temperature;

    /**
     * @Groups("ressource")
     * @ORM\Column(type="float", nullable=true)
     */
    private $ressentie;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $direction_vent;

    /**
     * @Groups("ressource")
     */
    private $directionVent;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $vitesse_vent_kmh;

    /**
     * @Groups("ressource")
     */
    private $vitesseVentKmh;



    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\MeteoStation", inversedBy="meteoSpecifiques", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $meteo_commune;

    /**
     * @Groups("ressource")
     * @ORM\ManyToOne(targetEntity="App\Entity\PalierMeteo", inversedBy="meteoSpecifiques", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $palier_meteo;

    /**
     * @Groups("ressource")
     */
    private $palierMeteo;

    /**
     * @Groups("ressource")
     * @ORM\ManyToOne(targetEntity="App\Entity\RefMeteo", inversedBy="meteoSpecifiques", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $Description_temps;

    /**
     * @Groups("ressource")
     */
    private $DescriptionTemps;


    public function getId(): ?int
    {
        return $this->id;
    }


    public function getNeigeFraicheCm(): ?int
    {
        return $this->neige_fraiche_cm;
    }

    public function setNeigeFraicheCm(int $neige_fraiche_cm): self
    {
        $this->neige_fraiche_cm = $neige_fraiche_cm;

        return $this;
    }

    public function getTemperature(): ?float
    {
        return $this->temperature;
    }

    public function setTemperature(?float $temperature): self
    {
        $this->temperature = $temperature;

        return $this;
    }

    public function getRessentie(): ?float
    {
        return $this->ressentie;
    }

    public function setRessentie(?float $ressentie): self
    {
        $this->ressentie = $ressentie;

        return $this;
    }

    public function getDirectionVent(): ?string
    {
        return $this->direction_vent;
    }

    public function setDirectionVent(string $direction_vent): self
    {
        $this->direction_vent = $direction_vent;

        return $this;
    }

    public function getVitesseVentKmh(): ?float
    {
        return $this->vitesse_vent_kmh;
    }

    public function setVitesseVentKmh(?float $vitesse_vent_kmh): self
    {
        $this->vitesse_vent_kmh = $vitesse_vent_kmh;

        return $this;
    }

    public function getMeteoCommune(): ?MeteoStation
    {
        return $this->meteo_commune;
    }

    public function setMeteoCommune(?MeteoStation $meteo_commune): self
    {
        $this->meteo_commune = $meteo_commune;

        return $this;
    }

    public function getPalierMeteo(): ?PalierMeteo
    {
        return $this->palier_meteo;
    }

    public function setPalierMeteo(?PalierMeteo $palier_meteo): self
    {
        $this->palier_meteo = $palier_meteo;

        return $this;
    }

    public function getDescriptionTemps(): ?RefMeteo
    {
        return $this->Description_temps;
    }

    public function setDescriptionTemps(?RefMeteo $Description_temps): self
    {
        $this->Description_temps = $Description_temps;

        return $this;
    }
}
