<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource(
 *     collectionOperations={"get"},
 *     itemOperations={"get"},
 *     normalizationContext={"groups"="ressource"}
 * )
 * @ApiFilter(SearchFilter::class, properties={"station": "exact" } )
 * @ApiFilter( DateFilter::class, properties={"Date"} )
 * @ORM\Entity(repositoryClass="App\Repository\MeteoStationRepository")
 */
class MeteoStation
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Groups("ressource")
     * @ORM\Column(type="date")
     */
    private $Date;

    /**
     * @Groups("ressource")
     * @ORM\Column(type="time")
     */
    private $Heure;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $niveau_gel_m;

    /**
     * @Groups("ressource")
     */
    private $niveauGelM;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $precipitation_mm;

    /**
     * @Groups("ressource")
     */
    private $precipitationMm;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $humidite_pct;

    /**
     * @Groups("ressource")
     */
    private $humiditePct;


    /**
     * @Groups("ressource")
     * @ORM\ManyToOne(targetEntity="App\Entity\Stations", inversedBy="meteo", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $station;

    /**
     * @Groups("ressource")
     * @ORM\OneToMany(targetEntity="App\Entity\MeteoSpecifique", mappedBy="meteo_commune")
     */
    private $meteoSpecifiques;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $couverture_nuageuse;

    /**
     * @Groups("ressource")
     */
    private $couvertureNuageuse;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $pluie_mm;

    /**
     * @Groups("ressource")
     */
    private $pluieMm;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $neige_mm;

    /**
     * @Groups("ressource")
     */
    private $neigeMm;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $raffale_vent_kmh;

    /**
     * @Groups("ressource")
     */
    private $raffaleVentKmh;

    /**
     * @Groups("ressource")
     * @ORM\OneToMany(targetEntity="App\Entity\IndiceSkiabilite", mappedBy="meteo_commune")
     */
    private $indiceSkiabilites;

    public function __construct()
    {
        $this->meteoSpecifiques = new ArrayCollection();
        $this->indiceSkiabilites = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->Date;
    }

    public function setDate(\DateTimeInterface $Date): self
    {
        $this->Date = $Date;

        return $this;
    }

    public function getHeure(): ?\DateTimeInterface
    {
        return $this->Heure;
    }

    public function setHeure(\DateTimeInterface $Heure): self
    {
        $this->Heure = $Heure;

        return $this;
    }

    public function getNiveauGelM(): ?int
    {
        return $this->niveau_gel_m;
    }

    public function setNiveauGelM(int $niveau_gel_m): self
    {
        $this->niveau_gel_m = $niveau_gel_m;

        return $this;
    }

    public function getPrecipitationMm(): ?int
    {
        return $this->precipitation_mm;
    }

    public function setPrecipitationMm(?int $precipitation_mm): self
    {
        $this->precipitation_mm = $precipitation_mm;

        return $this;
    }

    public function getHumiditePct(): ?int
    {
        return $this->humidite_pct;
    }

    public function setHumiditePct(?int $humidite_pct): self
    {
        $this->humidite_pct = $humidite_pct;

        return $this;
    }

    public function getStation(): ?Stations
    {
        return $this->station;
    }

    public function setStation(?Stations $station): self
    {
        $this->station = $station;

        return $this;
    }

    /**
     * @return Collection|MeteoSpecifique[]
     */
    public function getMeteoSpecifiques(): Collection
    {
        return $this->meteoSpecifiques;
    }

    public function addMeteoSpecifique(MeteoSpecifique $meteoSpecifique): self
    {
        if (!$this->meteoSpecifiques->contains($meteoSpecifique)) {
            $this->meteoSpecifiques[] = $meteoSpecifique;
            $meteoSpecifique->setMeteoCommune($this);
        }

        return $this;
    }

    public function removeMeteoSpecifique(MeteoSpecifique $meteoSpecifique): self
    {
        if ($this->meteoSpecifiques->contains($meteoSpecifique)) {
            $this->meteoSpecifiques->removeElement($meteoSpecifique);
            // set the owning side to null (unless already changed)
            if ($meteoSpecifique->getMeteoCommune() === $this) {
                $meteoSpecifique->setMeteoCommune(null);
            }
        }

        return $this;
    }

    public function getCouvertureNuageuse(): ?int
    {
        return $this->couverture_nuageuse;
    }

    public function setCouvertureNuageuse(?int $couverture_nuageuse): self
    {
        $this->couverture_nuageuse = $couverture_nuageuse;

        return $this;
    }

    public function getPluieMm(): ?int
    {
        return $this->pluie_mm;
    }

    public function setPluieMm(?int $pluie_mm): self
    {
        $this->pluie_mm = $pluie_mm;

        return $this;
    }

    public function getNeigeMm(): ?int
    {
        return $this->neige_mm;
    }

    public function setNeigeMm(?int $neige_mm): self
    {
        $this->neige_mm = $neige_mm;

        return $this;
    }
    public function getRaffaleVentKmh(): ?string
    {
        return $this->raffale_vent_kmh;
    }

    public function setRaffaleVentKmh(string $raffale_vent_kmh): self
    {
        $this->raffale_vent_kmh = $raffale_vent_kmh;

        return $this;
    }

    /**
     * @return Collection|IndiceSkiabilite[]
     */
    public function getIndiceSkiabilites(): Collection
    {
        return $this->indiceSkiabilites;
    }

    public function addIndiceSkiabilite(IndiceSkiabilite $indiceSkiabilite): self
    {
        if (!$this->indiceSkiabilites->contains($indiceSkiabilite)) {
            $this->indiceSkiabilites[] = $indiceSkiabilite;
            $indiceSkiabilite->setMeteoCommune($this);
        }

        return $this;
    }

    public function removeIndiceSkiabilite(IndiceSkiabilite $indiceSkiabilite): self
    {
        if ($this->indiceSkiabilites->contains($indiceSkiabilite)) {
            $this->indiceSkiabilites->removeElement($indiceSkiabilite);
            // set the owning side to null (unless already changed)
            if ($indiceSkiabilite->getMeteoCommune() === $this) {
                $indiceSkiabilite->setMeteoCommune(null);
            }
        }

        return $this;
    }

}
