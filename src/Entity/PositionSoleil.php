<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\DateFilter;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource(
 *     collectionOperations={"get"},
 *     itemOperations={"get"}
 * )
 * @ApiFilter( DateFilter::class, properties={"Date"} )
 * @ORM\Entity(repositoryClass="App\Repository\PositionSoleilRepository")
 */
class PositionSoleil
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="date")
     */
    private $Date;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $E_5;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $A_5;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $E_6;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $A_6;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $E_7;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $A_7;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $E_8;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $A_8;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $E_9;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $A_9;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $E_10;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $A_10;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $E_11;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $A_11;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $E_12;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $A_12;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $E_13;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $A_13;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $E_14;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $A_14;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $E_15;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $A_15;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $E_16;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $A_16;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $E_17;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $A_17;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $E_18;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $A_18;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $E_19;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $A_19;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $E_20;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $A_20;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDate()
    {
        return $this->Date;
    }

    public function setDate(\DateTimeInterface $Date): self
    {
        $this->Date = $Date;

        return $this;
    }

    public function getE5(): ?float
    {
        return $this->E_5;
    }

    public function setE5(?float $E_5): self
    {
        $this->E_5 = $E_5;

        return $this;
    }

    public function getA5(): ?float
    {
        return $this->A_5;
    }

    public function setA5(?float $A_5): self
    {
        $this->A_5 = $A_5;

        return $this;
    }

    public function getE6(): ?float
    {
        return $this->E_6;
    }

    public function setE6(float $E_6): self
    {
        $this->E_6 = $E_6;

        return $this;
    }

    public function getA6(): ?float
    {
        return $this->A_6;
    }

    public function setA6(?float $A_6): self
    {
        $this->A_6 = $A_6;

        return $this;
    }

    public function getE7(): ?float
    {
        return $this->E_7;
    }

    public function setE7(?float $E_7): self
    {
        $this->E_7 = $E_7;

        return $this;
    }

    public function getA7(): ?float
    {
        return $this->A_7;
    }

    public function setA7(?float $A_7): self
    {
        $this->A_7 = $A_7;

        return $this;
    }

    public function getE8(): ?float
    {
        return $this->E_8;
    }

    public function setE8(?float $E_8): self
    {
        $this->E_8 = $E_8;

        return $this;
    }

    public function getA8(): ?float
    {
        return $this->A_8;
    }

    public function setA8(?float $A_8): self
    {
        $this->A_8 = $A_8;

        return $this;
    }

    public function getE9(): ?float
    {
        return $this->E_9;
    }

    public function setE9(?float $E_9): self
    {
        $this->E_9 = $E_9;

        return $this;
    }

    public function getA9(): ?float
    {
        return $this->A_9;
    }

    public function setA9(?float $A_9): self
    {
        $this->A_9 = $A_9;

        return $this;
    }

    public function getE10(): ?float
    {
        return $this->E_10;
    }

    public function setE10(?float $E_10): self
    {
        $this->E_10 = $E_10;

        return $this;
    }

    public function getA10(): ?float
    {
        return $this->A_10;
    }

    public function setA10(?float $A_10): self
    {
        $this->A_10 = $A_10;

        return $this;
    }

    public function getE11(): ?float
    {
        return $this->E_11;
    }

    public function setE11(?float $E_11): self
    {
        $this->E_11 = $E_11;

        return $this;
    }

    public function getA11(): ?float
    {
        return $this->A_11;
    }

    public function setA11(?float $A_11): self
    {
        $this->A_11 = $A_11;

        return $this;
    }

    public function getE12(): ?float
    {
        return $this->E_12;
    }

    public function setE12(?float $E_12): self
    {
        $this->E_12 = $E_12;

        return $this;
    }

    public function getA12(): ?float
    {
        return $this->A_12;
    }

    public function setA12(?float $A_12): self
    {
        $this->A_12 = $A_12;

        return $this;
    }

    public function getE13(): ?float
    {
        return $this->E_13;
    }

    public function setE13(?float $E_13): self
    {
        $this->E_13 = $E_13;

        return $this;
    }

    public function getA13(): ?float
    {
        return $this->A_13;
    }

    public function setA13(?float $A_13): self
    {
        $this->A_13 = $A_13;

        return $this;
    }

    public function getE14(): ?float
    {
        return $this->E_14;
    }

    public function setE14(?float $E_14): self
    {
        $this->E_14 = $E_14;

        return $this;
    }

    public function getA14(): ?float
    {
        return $this->A_14;
    }

    public function setA14(?float $A_14): self
    {
        $this->A_14 = $A_14;

        return $this;
    }

    public function getE15(): ?float
    {
        return $this->E_15;
    }

    public function setE15(?float $E_15): self
    {
        $this->E_15 = $E_15;

        return $this;
    }

    public function getA15(): ?float
    {
        return $this->A_15;
    }

    public function setA15(?float $A_15): self
    {
        $this->A_15 = $A_15;

        return $this;
    }

    public function getE16(): ?float
    {
        return $this->E_16;
    }

    public function setE16(float $E_16): self
    {
        $this->E_16 = $E_16;

        return $this;
    }

    public function getA16(): ?float
    {
        return $this->A_16;
    }

    public function setA16(?float $A_16): self
    {
        $this->A_16 = $A_16;

        return $this;
    }

    public function getE17(): ?float
    {
        return $this->E_17;
    }

    public function setE17(?float $E_17): self
    {
        $this->E_17 = $E_17;

        return $this;
    }

    public function getA17(): ?float
    {
        return $this->A_17;
    }

    public function setA17(?float $A_17): self
    {
        $this->A_17 = $A_17;

        return $this;
    }

    public function getE18(): ?float
    {
        return $this->E_18;
    }

    public function setE18(?float $E_18): self
    {
        $this->E_18 = $E_18;

        return $this;
    }

    public function getA18(): ?float
    {
        return $this->A_18;
    }

    public function setA18(?float $A_18): self
    {
        $this->A_18 = $A_18;

        return $this;
    }

    public function getE19(): ?float
    {
        return $this->E_19;
    }

    public function setE19(?float $E_19): self
    {
        $this->E_19 = $E_19;

        return $this;
    }

    public function getA19(): ?float
    {
        return $this->A_19;
    }

    public function setA19(?float $A_19): self
    {
        $this->A_19 = $A_19;

        return $this;
    }

    public function getE20(): ?float
    {
        return $this->E_20;
    }

    public function setE20(?float $E_20): self
    {
        $this->E_20 = $E_20;

        return $this;
    }

    public function getA20(): ?float
    {
        return $this->A_20;
    }

    public function setA20(?float $A_20): self
    {
        $this->A_20 = $A_20;

        return $this;
    }
}
