<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource(
 *     collectionOperations={"get"},
 *     itemOperations={"get"}
 * )
 * @ORM\Entity(repositoryClass="App\Repository\PalierRefPisteRepository")
 */
class PalierRefPiste
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Groups("ressource")
     * @ORM\Column(type="string", length=255)
     */
    private $type;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Voie", mappedBy="palierRef")
     */
    private $voies;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\IndiceSkiabilite", mappedBy="PalierRef")
     */
    private $indiceSkiabilites;

    public function __construct()
    {
        $this->voies = new ArrayCollection();
        $this->indiceSkiabilites = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return Collection|Voie[]
     */
    public function getVoies(): Collection
    {
        return $this->voies;
    }

    public function addVoie(Voie $voie): self
    {
        if (!$this->voies->contains($voie)) {
            $this->voies[] = $voie;
            $voie->setPalierRef($this);
        }

        return $this;
    }

    public function removeVoie(Voie $voie): self
    {
        if ($this->voies->contains($voie)) {
            $this->voies->removeElement($voie);
            // set the owning side to null (unless already changed)
            if ($voie->getPalierRef() === $this) {
                $voie->setPalierRef(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|IndiceSkiabilite[]
     */
    public function getIndiceSkiabilites(): Collection
    {
        return $this->indiceSkiabilites;
    }

    public function addIndiceSkiabilite(IndiceSkiabilite $indiceSkiabilite): self
    {
        if (!$this->indiceSkiabilites->contains($indiceSkiabilite)) {
            $this->indiceSkiabilites[] = $indiceSkiabilite;
            $indiceSkiabilite->setPalierRef($this);
        }

        return $this;
    }

    public function removeIndiceSkiabilite(IndiceSkiabilite $indiceSkiabilite): self
    {
        if ($this->indiceSkiabilites->contains($indiceSkiabilite)) {
            $this->indiceSkiabilites->removeElement($indiceSkiabilite);
            // set the owning side to null (unless already changed)
            if ($indiceSkiabilite->getPalierRef() === $this) {
                $indiceSkiabilite->setPalierRef(null);
            }
        }

        return $this;
    }
}
