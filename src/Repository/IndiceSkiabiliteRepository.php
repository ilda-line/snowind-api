<?php

namespace App\Repository;

use App\Entity\IndiceSkiabilite;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method IndiceSkiabilite|null find($id, $lockMode = null, $lockVersion = null)
 * @method IndiceSkiabilite|null findOneBy(array $criteria, array $orderBy = null)
 * @method IndiceSkiabilite[]    findAll()
 * @method IndiceSkiabilite[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class IndiceSkiabiliteRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, IndiceSkiabilite::class);
    }

    // /**
    //  * @return IndiceSkiabilite[] Returns an array of IndiceSkiabilite objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?IndiceSkiabilite
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
