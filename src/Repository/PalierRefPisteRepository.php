<?php

namespace App\Repository;

use App\Entity\PalierRefPiste;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method PalierRefPiste|null find($id, $lockMode = null, $lockVersion = null)
 * @method PalierRefPiste|null findOneBy(array $criteria, array $orderBy = null)
 * @method PalierRefPiste[]    findAll()
 * @method PalierRefPiste[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PalierRefPisteRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, PalierRefPiste::class);
    }

    // /**
    //  * @return PalierRefPiste[] Returns an array of PalierRefPiste objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PalierRefPiste
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
