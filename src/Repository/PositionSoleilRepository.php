<?php

namespace App\Repository;

use App\Entity\PositionSoleil;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method PositionSoleil|null find($id, $lockMode = null, $lockVersion = null)
 * @method PositionSoleil|null findOneBy(array $criteria, array $orderBy = null)
 * @method PositionSoleil[]    findAll()
 * @method PositionSoleil[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PositionSoleilRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, PositionSoleil::class);
    }

    // /**
    //  * @return PositionSoleil[] Returns an array of PositionSoleil objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PositionSoleil
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
