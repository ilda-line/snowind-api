<?php

namespace App\Repository;

use App\Entity\CategoryVoie;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method CategoryVoie|null find($id, $lockMode = null, $lockVersion = null)
 * @method CategoryVoie|null findOneBy(array $criteria, array $orderBy = null)
 * @method CategoryVoie[]    findAll()
 * @method CategoryVoie[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CategoryVoieRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, CategoryVoie::class);
    }

    // /**
    //  * @return CategoryVoie[] Returns an array of CategoryVoie objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CategoryVoie
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
