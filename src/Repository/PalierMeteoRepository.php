<?php

namespace App\Repository;

use App\Entity\PalierMeteo;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method PalierMeteo|null find($id, $lockMode = null, $lockVersion = null)
 * @method PalierMeteo|null findOneBy(array $criteria, array $orderBy = null)
 * @method PalierMeteo[]    findAll()
 * @method PalierMeteo[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PalierMeteoRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, PalierMeteo::class);
    }

    // /**
    //  * @return PalierMeteo[] Returns an array of PalierMeteo objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PalierMeteo
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
