<?php

namespace App\Repository;

use App\Entity\MeteoStation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method MeteoStation|null find($id, $lockMode = null, $lockVersion = null)
 * @method MeteoStation|null findOneBy(array $criteria, array $orderBy = null)
 * @method MeteoStation[]    findAll()
 * @method MeteoStation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MeteoStationRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, MeteoStation::class);
    }

    // /**
    //  * @return MeteoStation[] Returns an array of MeteoStation objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?MeteoStation
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
