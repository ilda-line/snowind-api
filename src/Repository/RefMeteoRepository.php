<?php

namespace App\Repository;

use App\Entity\RefMeteo;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method RefMeteo|null find($id, $lockMode = null, $lockVersion = null)
 * @method RefMeteo|null findOneBy(array $criteria, array $orderBy = null)
 * @method RefMeteo[]    findAll()
 * @method RefMeteo[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RefMeteoRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, RefMeteo::class);
    }

    // /**
    //  * @return RefMeteo[] Returns an array of RefMeteo objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?RefMeteo
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
