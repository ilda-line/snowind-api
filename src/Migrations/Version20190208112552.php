<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190208112552 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE position_soleil ADD date DATE NOT NULL, ADD e_5 DOUBLE PRECISION DEFAULT NULL, ADD a_5 DOUBLE PRECISION DEFAULT NULL, ADD e_6 DOUBLE PRECISION DEFAULT NULL, ADD a_6 DOUBLE PRECISION DEFAULT NULL, ADD e_7 DOUBLE PRECISION DEFAULT NULL, ADD a_7 DOUBLE PRECISION DEFAULT NULL, ADD e_8 DOUBLE PRECISION DEFAULT NULL, ADD a_8 DOUBLE PRECISION DEFAULT NULL, ADD e_9 DOUBLE PRECISION DEFAULT NULL, ADD a_9 DOUBLE PRECISION DEFAULT NULL, ADD e_10 DOUBLE PRECISION DEFAULT NULL, ADD a_10 DOUBLE PRECISION DEFAULT NULL, ADD e_11 DOUBLE PRECISION DEFAULT NULL, ADD a_11 DOUBLE PRECISION DEFAULT NULL, ADD e_12 DOUBLE PRECISION DEFAULT NULL, ADD a_12 DOUBLE PRECISION DEFAULT NULL, ADD e_13 DOUBLE PRECISION DEFAULT NULL, ADD a_13 DOUBLE PRECISION DEFAULT NULL, ADD e_14 DOUBLE PRECISION DEFAULT NULL, ADD a_14 DOUBLE PRECISION DEFAULT NULL, ADD e_15 DOUBLE PRECISION DEFAULT NULL, ADD a_15 DOUBLE PRECISION DEFAULT NULL, ADD e_16 DOUBLE PRECISION DEFAULT NULL, ADD a_16 DOUBLE PRECISION DEFAULT NULL, ADD e_17 DOUBLE PRECISION DEFAULT NULL, ADD a_17 DOUBLE PRECISION DEFAULT NULL, ADD e_18 DOUBLE PRECISION DEFAULT NULL, ADD a_18 DOUBLE PRECISION DEFAULT NULL, ADD e_19 DOUBLE PRECISION DEFAULT NULL, ADD a_19 DOUBLE PRECISION DEFAULT NULL, ADD e_20 DOUBLE PRECISION DEFAULT NULL, ADD a_20 DOUBLE PRECISION DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE position_soleil DROP date, DROP e_5, DROP a_5, DROP e_6, DROP a_6, DROP e_7, DROP a_7, DROP e_8, DROP a_8, DROP e_9, DROP a_9, DROP e_10, DROP a_10, DROP e_11, DROP a_11, DROP e_12, DROP a_12, DROP e_13, DROP a_13, DROP e_14, DROP a_14, DROP e_15, DROP a_15, DROP e_16, DROP a_16, DROP e_17, DROP a_17, DROP e_18, DROP a_18, DROP e_19, DROP a_19, DROP e_20, DROP a_20');
    }
}
