<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190213165052 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE meteo_station (id INT AUTO_INCREMENT NOT NULL, station_id INT NOT NULL, date DATE NOT NULL, heure TIME NOT NULL, niveau_gel_m INT NOT NULL, precipitation_mm INT DEFAULT NULL, humidite_pct INT DEFAULT NULL, description_temps VARCHAR(255) NOT NULL, neige_fraiche_cm INT NOT NULL, temperature DOUBLE PRECISION NOT NULL, ressentie DOUBLE PRECISION NOT NULL, direction_vent VARCHAR(10) NOT NULL, vitesse_vent_kmh DOUBLE PRECISION NOT NULL, raffale_vent_kmh VARCHAR(255) NOT NULL, INDEX IDX_82E6812F21BDB235 (station_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE voie (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) NOT NULL, etat VARCHAR(255) NOT NULL, points JSON NOT NULL, category VARCHAR(50) NOT NULL, frequentation INT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE meteo_station ADD CONSTRAINT FK_82E6812F21BDB235 FOREIGN KEY (station_id) REFERENCES stations (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE meteo_station');
        $this->addSql('DROP TABLE voie');
    }
}
