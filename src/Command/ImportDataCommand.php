<?php
namespace App\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Helper\ProgressBar;

use App\Entity\PositionSoleil;

class ImportDataCommand extends ContainerAwareCommand
{
    // the name of the command (the part after "bin/console")

    protected function configure()
    {
        // Name and description for app/console command
        $this
            ->setName('import:csv')
            ->setDescription('Import users from CSV file');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // Showing when the script is launched
        $now = new \DateTime();
        $output->writeln('<comment>Start : ' . $now->format('d-m-Y G:i:s') . ' ---</comment>');

        // Importing CSV on DB via Doctrine ORM
        $this->import($input, $output);

        // Showing when the script is over
        $now = new \DateTime();
        $output->writeln('<comment>End : ' . $now->format('d-m-Y G:i:s') . ' ---</comment>');
    }

    protected function import(InputInterface $input, OutputInterface $output)
    {
        // Getting php array of data from CSV
        $data = $this->get($input, $output);
        // Getting doctrine manager
        $em = $this->getContainer()->get('doctrine')->getManager();
        // Turning off doctrine default logs queries for saving memory
        $em->getConnection()->getConfiguration()->setSQLLogger(null);

        // Define the size of record, the frequency for persisting the data and the current index of records
        $size = count($data);
        $batchSize = 20;
        $i = 1;

        // Starting progress
        $progress = new ProgressBar($output, $size);
        $progress->start();


        // Processing on each row of data
        foreach($data as $row) {

            $nb = 5;

            while($nb<21){


                if( $row['E_'.$nb] == '--'){
                    $row['E_'.$nb] = 0 ;

                }else{
                   $row['E_'.$nb] = floatval($row['E_'.$nb]);
                }

                if( $row['A_'.$nb]== '--'){
                    $row['A_'.$nb] = 0 ;
                }else{
                    $row['A_'.$nb] = floatval($row['A_'.$nb]);
                }

                $nb ++;
            }


            $date = strtotime($row['date']);
            $date = date('Y-m-d ',$date);

            $user = new PositionSoleil();
            $user->setDate(new \DateTime($date));

                $user->setE5($row['E_5']);
                $user->setE6($row['E_6']);
                $user->setE7($row['E_7']);
                $user->setE8($row['E_8']);
                $user->setE9($row['E_9']);
                $user->setE10($row['E_10']);
                $user->setE11($row['E_11']);
                $user->setE12($row['E_12']);
                $user->setE13($row['E_13']);
                $user->setE14($row['E_14']);
                $user->setE15($row['E_15']);
                $user->setE16($row['E_16']);
                $user->setE17($row['E_17']);
                $user->setE18($row['E_18']);
                $user->setE19($row['E_19']);
                $user->setE20($row['E_20']);

                $user->setA5($row['A_5']);
                $user->setA6($row['A_6']);
                $user->setA7($row['A_7']);
                $user->setA8($row['A_8']);
                $user->setA9($row['A_9']);
                $user->setA10($row['A_10']);
                $user->setA11($row['A_11']);
                $user->setA12($row['A_12']);
                $user->setA13($row['A_13']);
                $user->setA14($row['A_14']);
                $user->setA15($row['A_15']);
                $user->setA16($row['A_16']);
                $user->setA17($row['A_17']);
                $user->setA18($row['A_18']);
                $user->setA19($row['A_19']);
                $user->setA20($row['A_20']);



            // Persisting the current user
            $em->persist($user);

            // Each 20 users persisted we flush everything
            if (($i % $batchSize) === 0) {

                $em->flush();
                // Detaches all objects from Doctrine for memory save
                $em->clear();

                // Advancing for progress display on console
                $progress->advance($batchSize);

                $now = new \DateTime();
                $output->writeln(' of users imported ... | ' . $now->format('d-m-Y G:i:s'));

            }

            $i++;

        }

        // Flushing and clear data on queue
        $em->flush();
        $em->clear();

        // Ending the progress bar process
        $progress->finish();
    }

    protected function get(InputInterface $input, OutputInterface $output)
    {
        // Getting the CSV from filesystem
        $fileName = 'fixtures/donnee_solaire.csv';

        // Using service for converting CSV to PHP Array
        $converter = $this->getContainer()->get('import.csvtoarray');
        $data = $converter->convert($fileName, ';');
        return $data;
    }
}