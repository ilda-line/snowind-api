<?php
namespace App\Command;


use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;


use App\Entity\RefMeteo;
use App\Entity\MeteoStation;
use App\Entity\MeteoSpecifique;
use App\Entity\Stations;
use App\Entity\PalierMeteo;


class cronMeteo extends ContainerAwareCommand
{
    // the name of the command (the part after "bin/console")

    protected function configure()
    {
        // Name and description for app/console command
        $this
            ->setName('cron:meteo')
            ->setDescription('Recupere les données de l API');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // Showing when the script is launched
        // Importing CSV on DB via Doctrine ORM
        $this->import($input, $output);

        // Showing when the script is over
        $now = new \DateTime();
        $output->writeln('<comment>End : ' . $now->format('d-m-Y G:i:s') . ' ---</comment>');
    }

    protected function import(InputInterface $input, OutputInterface $output)
    {
        // Getting php array of data from CSV
        $json = $this->get($input, $output);
        // Getting doctrine manager
        $em = $this->getContainer()->get('doctrine')->getManager();
        // Turning off doctrine default logs queries for saving memory
        $em->getConnection()->getConfiguration()->setSQLLogger(null);

        $data =  json_decode($json, true);
        // Define the size of record, the frequency for persisting the data and the current index of records
        $bashsize = 20;
        $i = 0;


        $repository = $this->getContainer()->get('doctrine')->getRepository(Stations::class);
        $id_stations  = $repository->findOneBy(['id_API' => $data['id']]);

        $repository = $this->getContainer()->get('doctrine')->getRepository(PalierMeteo::class);
        $base  = $repository->findOneBy(['type' => 'Bas']);
        $milieu  = $repository->findOneBy(['type' => 'Milieu']);
        $haut  = $repository->findOneBy(['type' => 'Haut']);




        // On parcourt tout le tableau de data ligne par ligne
        foreach($data['forecast'] as $key => $row) {

            $dateformat = str_replace("/", "-", $row['date'] );
            $timeStamp = strtotime($dateformat);
            $date = new \DateTime(date('d-m-Y', $timeStamp ));
            $time = new \DateTime($row['time']);

            $repository = $this->getContainer()->get('doctrine')->getRepository(MeteoStation::class);
            $meteo_existe  = $repository->findOneBy(['Date' => $date, 'Heure' => $time]);

            // On vérifie si la ligne existe déjà
            if($meteo_existe){
                // On la met à jour
                // TO DO regarder l'exitence de chaque champs 1 par 1 et mettre à jour seulement le necessaire
                $meteoCommune = $meteo_existe;
                $meteoCommune->setDate($date);
                $meteoCommune->setHeure($time);
                $meteoCommune->setCouvertureNuageuse($row['totalcloud_pct']);
                $meteoCommune->setHumiditePct($row['hum_pct']);
                $meteoCommune->setNeigeMm($row['snow_mm']);
                $meteoCommune->setPluieMm($row['rain_mm']);
                $meteoCommune->setNiveauGelM($row['frzglvl_m']);
                $meteoCommune->setPrecipitationMm($row['precip_mm']);
                $meteoCommune->setStation($id_stations);
                $meteoCommune->setRaffaleVentKmh($row['base']['windgst_kmh']);


                $em->persist($meteoCommune);
                $em->flush();


                $repository = $this->getContainer()->get('doctrine')->getRepository(MeteoSpecifique::class);
                $meteo_spe  = $repository->findOneBy(['meteo_commune' => $meteo_existe]);


                $repository = $this->getContainer()->get('doctrine')->getRepository(RefMeteo::class);
                $refMeteo_base  = $repository->findOneBy(['id' => $row['base']['wx_code']]);
                $refMeteo_milieu  = $repository->findOneBy(['id' => $row['mid']['wx_code']]);
                $refMeteo_haut  = $repository->findOneBy(['id' => $row['upper']['wx_code']]);

                $meteoSpecifique_base = $meteo_spe;
                $meteoSpecifique_base->setDescriptionTemps($refMeteo_base);
                $meteoSpecifique_base->setDirectionVent($row['base']['winddir_compass']);
                $meteoSpecifique_base->setVitesseVentKmh($row['base']['windspd_kmh']);
                $meteoSpecifique_base->setTemperature($row['base']['temp_c']);
                $meteoSpecifique_base->setRessentie($row['base']['feelslike_c']);
                $meteoSpecifique_base->setNeigeFraicheCm($row['base']['freshsnow_cm']);
                $meteoSpecifique_base->setPalierMeteo($base);
                $meteoSpecifique_base->setMeteoCommune($meteo_existe);

                $meteoSpecifique_milieu = $meteo_spe;
                $meteoSpecifique_milieu->setDescriptionTemps($refMeteo_milieu);
                $meteoSpecifique_milieu->setDirectionVent($row['mid']['winddir_compass']);
                $meteoSpecifique_milieu->setVitesseVentKmh($row['mid']['windspd_kmh']);
                $meteoSpecifique_milieu->setTemperature($row['mid']['temp_c']);
                $meteoSpecifique_milieu->setRessentie($row['mid']['feelslike_c']);
                $meteoSpecifique_milieu->setNeigeFraicheCm($row['mid']['freshsnow_cm']);
                $meteoSpecifique_milieu->setPalierMeteo($milieu);
                $meteoSpecifique_milieu->setMeteoCommune($meteo_existe);


                $meteoSpecifique_haut = $meteo_spe;
                $meteoSpecifique_haut->setDescriptionTemps($refMeteo_haut);
                $meteoSpecifique_haut->setDirectionVent($row['upper']['winddir_compass']);
                $meteoSpecifique_haut->setVitesseVentKmh($row['upper']['windspd_kmh']);
                $meteoSpecifique_haut->setTemperature($row['upper']['temp_c']);
                $meteoSpecifique_haut->setRessentie($row['upper']['feelslike_c']);
                $meteoSpecifique_haut->setNeigeFraicheCm($row['upper']['freshsnow_cm']);
                $meteoSpecifique_haut->setPalierMeteo($haut);
                $meteoSpecifique_haut->setMeteoCommune($meteo_existe);

                // Persisting the current meteo commune
                $em->persist($meteoSpecifique_base);
                $em->persist( $meteoSpecifique_milieu );
                $em->persist($meteoSpecifique_haut );

            }else{

                // On la créer
                $output->writeln('<comment>End : ' . 'ici' . ' ---</comment>');


                $meteoCommune = new MeteoStation();
                $meteoCommune->setDate($date);
                $meteoCommune->setHeure($time);
                $meteoCommune->setCouvertureNuageuse($row['totalcloud_pct']);
                $meteoCommune->setHumiditePct($row['hum_pct']);
                $meteoCommune->setNeigeMm($row['snow_mm']);
                $meteoCommune->setPluieMm($row['rain_mm']);
                $meteoCommune->setNiveauGelM($row['frzglvl_m']);
                $meteoCommune->setPrecipitationMm($row['precip_mm']);
                $meteoCommune->setStation($id_stations);
                $meteoCommune->setRaffaleVentKmh($row['base']['windgst_kmh']);


                $em->persist($meteoCommune);
                $em->flush();


                $repository = $this->getContainer()->get('doctrine')->getRepository(MeteoStation::class);
                $id_meteo_commune  = $repository->findOneBy(['Date' => $date, 'Heure' => $time]);


                $repository = $this->getContainer()->get('doctrine')->getRepository(RefMeteo::class);
                $refMeteo_base  = $repository->findOneBy(['id' => $row['base']['wx_code']]);
                $refMeteo_milieu  = $repository->findOneBy(['id' => $row['mid']['wx_code']]);
                $refMeteo_haut  = $repository->findOneBy(['id' => $row['upper']['wx_code']]);

                $meteoSpecifique_base = new MeteoSpecifique();
                $meteoSpecifique_base->setDescriptionTemps($refMeteo_base);
                $meteoSpecifique_base->setDirectionVent($row['base']['winddir_compass']);
                $meteoSpecifique_base->setVitesseVentKmh($row['base']['windspd_kmh']);
                $meteoSpecifique_base->setTemperature($row['base']['temp_c']);
                $meteoSpecifique_base->setRessentie($row['base']['feelslike_c']);
                $meteoSpecifique_base->setNeigeFraicheCm($row['base']['freshsnow_cm']);
                $meteoSpecifique_base->setPalierMeteo($base);
                $meteoSpecifique_base->setMeteoCommune($id_meteo_commune);

                $meteoSpecifique_milieu = new MeteoSpecifique();
                $meteoSpecifique_milieu->setDescriptionTemps($refMeteo_milieu);
                $meteoSpecifique_milieu->setDirectionVent($row['mid']['winddir_compass']);
                $meteoSpecifique_milieu->setVitesseVentKmh($row['mid']['windspd_kmh']);
                $meteoSpecifique_milieu->setTemperature($row['mid']['temp_c']);
                $meteoSpecifique_milieu->setRessentie($row['mid']['feelslike_c']);
                $meteoSpecifique_milieu->setNeigeFraicheCm($row['mid']['freshsnow_cm']);
                $meteoSpecifique_milieu->setPalierMeteo($milieu);
                $meteoSpecifique_milieu->setMeteoCommune($id_meteo_commune);


                $meteoSpecifique_haut = new MeteoSpecifique();
                $meteoSpecifique_haut->setDescriptionTemps($refMeteo_haut);
                $meteoSpecifique_haut->setDirectionVent($row['upper']['winddir_compass']);
                $meteoSpecifique_haut->setVitesseVentKmh($row['upper']['windspd_kmh']);
                $meteoSpecifique_haut->setTemperature($row['upper']['temp_c']);
                $meteoSpecifique_haut->setRessentie($row['upper']['feelslike_c']);
                $meteoSpecifique_haut->setNeigeFraicheCm($row['upper']['freshsnow_cm']);
                $meteoSpecifique_haut->setPalierMeteo($haut);
                $meteoSpecifique_haut->setMeteoCommune($id_meteo_commune);

                // Persisting the current meteo commune
                $em->persist($meteoSpecifique_base);
                $em->persist( $meteoSpecifique_milieu );
                $em->persist($meteoSpecifique_haut );


            }


            // Quand on a enregistré 20 lignes on les enregistrent
            if ($i == $bashsize ) {

                $em->flush();
                // On nettoie tous les objets enregitrées dans Doctrine pour libéré de la mémoire
                $em->clear();

                // on remet le compteur a 0
                $i=0;

                 // on reset les valeurs utiles qui ont été supprimer par le clear()
                $repository = $this->getContainer()->get('doctrine')->getRepository(Stations::class);
                $id_stations  = $repository->findOneBy(['id_API' => $data['id']]);

                $repository = $this->getContainer()->get('doctrine')->getRepository(PalierMeteo::class);
                $base  = $repository->findOneBy(['type' => 'Bas']);
                $milieu  = $repository->findOneBy(['type' => 'Milieu']);
                $haut  = $repository->findOneBy(['type' => 'Haut']);


                $now = new \DateTime();
                $output->writeln(' of users imported ... | ' . $now->format('d-m-Y G:i:s'));

            }

            $i++;

        }

        // On flush tout ce qu'il reste dans la queue et on nettoie le cache
        $em->flush();
        $em->clear();

    }
    protected function get(InputInterface $input, OutputInterface $output)
    {
        // create curl resource
        $ch = curl_init();

        // set url
        curl_setopt($ch, CURLOPT_URL, "https://api.weatherunlocked.com/api/resortforecast/54883730?app_id=52d26658&app_key=87802b2f1b70f1ec8a1c91acc39143db");

        //return the transfer as a string
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json')
        );

        // $output contains the output string
        $output = curl_exec($ch);

        // close curl resource to free up system resources
        curl_close($ch);

        return $output;
    }
}
