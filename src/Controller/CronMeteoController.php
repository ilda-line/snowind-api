<?php
namespace App\Controller;

use App\Entity\IndiceSkiabilite;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

use App\Entity\RefMeteo;
use App\Entity\MeteoStation;
use App\Entity\MeteoSpecifique;
use App\Entity\Stations;
use App\Entity\PalierMeteo;
use App\Entity\PalierRefPiste;


class CronMeteoController extends Controller
{

    /**
     * @Route("/cronMeteo", name="cron")
     */
    public function index()
    {
        $data = $this->getData();
        $this->importData($data);

        return $this->render('base.html.twig');
    }


    protected function importData($json)
    {
        // Getting doctrine manager
        $em = $this->getDoctrine()->getManager();
        // Turning off doctrine default logs queries for saving memory
        $em->getConnection()->getConfiguration()->setSQLLogger(null);

        $data =  json_decode($json, true);
        // Define the size of record, the frequency for persisting the data and the current index of records
        $bashsize = 20;
        $i = 0;


        $repository = $this->getDoctrine()->getRepository(Stations::class);
        $id_stations  = $repository->findOneBy(['id_API' => $data['id']]);


        $repository = $this->getDoctrine()->getRepository(PalierMeteo::class);
        $base  = $repository->findOneBy(['type' => 'Bas']);
        $milieu  = $repository->findOneBy(['type' => 'Milieu']);
        $haut  = $repository->findOneBy(['type' => 'Haut']);


        // On parcourt tout le tableau de data ligne par ligne
        foreach($data['forecast'] as $key => $row) {

            $dateformat = str_replace("/", "-", $row['date'] );
            $timeStamp = strtotime($dateformat);
            $date = new \DateTime(date('d-m-Y', $timeStamp ));
            $time = new \DateTime($row['time']);

            $repository = $this->getDoctrine()->getRepository(MeteoStation::class);
            $meteo_existe  = $repository->findOneBy(['Date' => $date, 'Heure' => $time]);

            // On vérifie si la ligne existe déjà
            if($meteo_existe){

                $meteoCommune = $meteo_existe;

                $repository = $this->getDoctrine()->getRepository(MeteoSpecifique::class);
                $meteo_spe_base  = $repository->findOneBy(['meteo_commune' => $meteo_existe, 'palier_meteo' => $base]);
                $repository = $this->getDoctrine()->getRepository(MeteoSpecifique::class);
                $meteo_spe_milieu  = $repository->findOneBy(['meteo_commune' => $meteo_existe, 'palier_meteo' => $milieu]);
                $repository = $this->getDoctrine()->getRepository(MeteoSpecifique::class);
                $meteo_spe_haut  = $repository->findOneBy(['meteo_commune' => $meteo_existe, 'palier_meteo' => $haut]);

            }
            //Sinon on créer une nouvelle
            else{
                $meteoCommune = new MeteoStation();
                $meteo_spe_base = $meteo_spe_milieu = $meteo_spe_haut = null;
            }

            // TO DO regarder l'exitence de chaque champs 1 par 1 et mettre à jour seulement le necessaire
            $meteoCommune->setDate($date);
            $meteoCommune->setHeure($time);
            $meteoCommune->setCouvertureNuageuse($row['totalcloud_pct']);
            $meteoCommune->setHumiditePct($row['hum_pct']);
            $meteoCommune->setNeigeMm($row['snow_mm']);
            $meteoCommune->setPluieMm($row['rain_mm']);
            $meteoCommune->setNiveauGelM($row['frzglvl_m']);
            $meteoCommune->setPrecipitationMm($row['precip_mm']);
            $meteoCommune->setStation($id_stations);
            $meteoCommune->setRaffaleVentKmh($row['base']['windgst_kmh']);

            $em->persist($meteoCommune);
            $em->flush();

            //METEO COMMUNE
            $repository = $this->getDoctrine()->getRepository(MeteoStation::class);
            $id_meteo_commune  = $repository->findOneBy(['Date' => $date, 'Heure' => $time]);

            $repository = $this->getDoctrine()->getRepository(RefMeteo::class);
            $refMeteo_base  = $repository->findOneBy(['id' => $row['base']['wx_code']]);
            $refMeteo_milieu  = $repository->findOneBy(['id' => $row['mid']['wx_code']]);
            $refMeteo_haut  = $repository->findOneBy(['id' => $row['upper']['wx_code']]);

            $meteoSpecifique_base = $this->verifierExistanceIndice($meteo_spe_base, $type = "meteospe");
            $meteoSpecifique_base->setDescriptionTemps($refMeteo_base);
            $meteoSpecifique_base->setDirectionVent($row['base']['winddir_compass']);
            $meteoSpecifique_base->setVitesseVentKmh($row['base']['windspd_kmh']);
            $meteoSpecifique_base->setTemperature($row['base']['temp_c']);
            $meteoSpecifique_base->setRessentie($row['base']['feelslike_c']);
            $meteoSpecifique_base->setNeigeFraicheCm($row['base']['freshsnow_cm']);
            $meteoSpecifique_base->setPalierMeteo($base);
            $meteoSpecifique_base->setMeteoCommune($id_meteo_commune);


            if($row['mid']['temp_c'] == NULL){
                $row['mid']['temp_c'] = round(($row['base']['temp_c'] +$row['upper']['temp_c'])/2 );
            }
            if($row['mid']['feelslike_c'] == NULL){
                $row['mid']['feelslike_c'] = round(($row['base']['feelslike_c'] +$row['upper']['feelslike_c'])/2);
            }

            $meteoSpecifique_milieu = $this->verifierExistanceIndice($meteo_spe_milieu, $type = "meteospe");
            $meteoSpecifique_milieu->setDescriptionTemps($refMeteo_milieu);
            $meteoSpecifique_milieu->setDirectionVent($row['mid']['winddir_compass']);
            $meteoSpecifique_milieu->setVitesseVentKmh($row['mid']['windspd_kmh']);
            $meteoSpecifique_milieu->setTemperature($row['mid']['temp_c']);
            $meteoSpecifique_milieu->setRessentie($row['mid']['feelslike_c']);
            $meteoSpecifique_milieu->setNeigeFraicheCm($row['mid']['freshsnow_cm']);
            $meteoSpecifique_milieu->setPalierMeteo($milieu);
            $meteoSpecifique_milieu->setMeteoCommune($id_meteo_commune);


            $meteoSpecifique_haut = $this->verifierExistanceIndice($meteo_spe_haut, $type = "meteospe");
            $meteoSpecifique_haut->setDescriptionTemps($refMeteo_haut);
            $meteoSpecifique_haut->setDirectionVent($row['upper']['winddir_compass']);
            $meteoSpecifique_haut->setVitesseVentKmh($row['upper']['windspd_kmh']);
            $meteoSpecifique_haut->setTemperature($row['upper']['temp_c']);
            $meteoSpecifique_haut->setRessentie($row['upper']['feelslike_c']);
            $meteoSpecifique_haut->setNeigeFraicheCm($row['upper']['freshsnow_cm']);
            $meteoSpecifique_haut->setPalierMeteo($haut);
            $meteoSpecifique_haut->setMeteoCommune($id_meteo_commune);

            // Persisting the current meteo commune
            $em->persist($meteoSpecifique_base);
            $em->persist( $meteoSpecifique_milieu );
            $em->persist($meteoSpecifique_haut );

            $this->calculerIndice($row,$refMeteo_base, $refMeteo_milieu, $refMeteo_haut,$id_meteo_commune);


            // Quand on a enregistré 20 lignes on les enregistrent
            if ($i == $bashsize ) {

                $em->flush();
                // On nettoie tous les objets enregitrées dans Doctrine pour libéré de la mémoire
                $em->clear();

                // on remet le compteur a 0
                $i=0;

                // on reset les valeurs utiles qui ont été supprimer par le clear()
                $repository = $this->getDoctrine()->getRepository(Stations::class);
                $id_stations  = $repository->findOneBy(['id_API' => $data['id']]);

                $repository = $this->getDoctrine()->getRepository(PalierMeteo::class);
                $base  = $repository->findOneBy(['type' => 'Bas']);
                $milieu  = $repository->findOneBy(['type' => 'Milieu']);
                $haut  = $repository->findOneBy(['type' => 'Haut']);


            }

            $i++;

        }

        // On flush tout ce qu'il reste dans la queue et on nettoie le cache
        $em->flush();
        $em->clear();
        echo 'OK';

    }
    protected function getData()
    {
        // create curl resource
        $ch = curl_init();

        // set url
        curl_setopt($ch, CURLOPT_URL, "https://api.weatherunlocked.com/api/resortforecast/54883730?app_id=52d26658&app_key=87802b2f1b70f1ec8a1c91acc39143db");

        //return the transfer as a string
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json')
        );

        // $output contains the output string
        $output = curl_exec($ch);

        // close curl resource to free up system resources
        curl_close($ch);

        return $output;
    }
    protected function verifierExistanceIndice($row_ind, $type)
    {
        // On vérifie qu'un objet est bien enregistré en correspondance avec l'id de météo
        if($row_ind == NULL ){
            // Si on en a pas on en créer un
            switch ($type){
                case "meteospe" :
                    $ind= new MeteoSpecifique();
                    break;
                case "indice" :
                    $ind= new IndiceSkiabilite();
                    break;
            }
        }else{
            $ind = $row_ind;
        }
        return $ind ;
    }

    protected function calculerIndice($row, $refMeteo_base, $refMeteo_milieu, $refMeteo_haut, $id_meteo ){

        $em = $this->getDoctrine()->getManager();

        $repository = $this->getDoctrine()->getRepository(PalierRefPiste::class);
        $base_piste  = $repository->findOneBy(['type' => 'Bas']);
        $milieu_piste  = $repository->findOneBy(['type' => 'Milieu']);
        $haut_piste  = $repository->findOneBy(['type' => 'Haut']);
        $bas_milieu_piste  = $repository->findOneBy(['type' => 'Bas-Milieu']);
        $milieu_haut_piste  = $repository->findOneBy(['type' => 'Milieu-Haut']);
        $bas_milieu_haut_piste  = $repository->findOneBy(['type' => 'Bas-Milieu-Haut']);

        // Indice de skiabilité
        $indice_generale = 0 ;
        $indice_bas = 0 ;
        $indice_milieu = 0 ;
        $indice_haut = 0 ;

        if(isset($row['base']['temp_c'] )){
            if( (-8 < $row['base']['temp_c'] && $row['base']['temp_c'] < -2 )|| (2 < $row['base']['temp_c'] && $row['base']['temp_c'] <=8) ){
                $indice_bas = $indice_bas + 1;
            }elseif (-2 <= $row['base']['temp_c'] && $row['base']['temp_c'] <= 2){
                $indice_bas = $indice_bas + 2;
            }
        }
        if(isset($row['base']['windspd_kmh'] )){
            if( 30 >= $row['base']['windspd_kmh'] && $row['base']['windspd_kmh']  ){
                $indice_bas = $indice_bas + 1;
            }
        }
        if(isset($row['mid']['temp_c'] )){
            if( (-8 < $row['mid']['temp_c'] && $row['mid']['temp_c'] < -2 )|| (2 < $row['mid']['temp_c'] && $row['mid']['temp_c'] <=8) ){
                $indice_milieu = $indice_milieu + 1;
            }elseif (-2 <= $row['mid']['temp_c'] && $row['mid']['temp_c'] <= 2){
                $indice_milieu = $indice_milieu + 2;
            }
        }
        if(isset($row['mid']['windspd_kmh'] )){
            if( 30 >= $row['mid']['windspd_kmh']  ){
                $indice_milieu = $indice_milieu + 1;
            }
        }
        if(isset($row['upper']['temp_c'] )){
            if( (-8 < $row['upper']['temp_c'] && $row['upper']['temp_c'] < -2 )|| (2 < $row['upper']['temp_c'] && $row['upper']['temp_c'] <=8) ){
                $indice_haut = $indice_haut + 1;
            }elseif (-2 <= $row['upper']['temp_c'] && $row['upper']['temp_c'] <= 2){
                $indice_haut = $indice_haut + 2;
            }
        }
        if(isset($row['upper']['windspd_kmh'] )){
            if( 30 >= $row['upper']['windspd_kmh'] && $row['upper']['windspd_kmh']  ){
                $indice_haut = $indice_haut + 1;
            }
        }


        $array_base = $refMeteo_base->getPointsIndice();
        $indice_bas = $indice_bas + $array_base ;
        $array_milieu = $refMeteo_milieu->getPointsIndice();
        $indice_milieu = $indice_milieu + $array_milieu ;
        $array_haut = $refMeteo_haut->getPointsIndice();
        $indice_haut = $indice_haut + $array_haut;

        if(isset($row['totalcloud_pct'])){
            if( 33 >= $row['totalcloud_pct']   ){
                $indice_generale = $indice_generale +2 ;
            }elseif( 66 >= $row['totalcloud_pct'] && 33 < $row['totalcloud_pct'] ){
                $indice_generale = $indice_generale + 1;
            }
        }
        if(isset($row['rain_mm'])){
            if( $row['rain_mm'] == 0  ){
                $indice_generale = $indice_generale +2 ;
            }elseif( 1 >= $row['rain_mm'] && 3 < $row['rain_mm'] ){
                $indice_generale = $indice_generale + 1;
            }
        }
        if(isset($row['base']['windgst_kmh'])){
            if( 30 >= $row['base']['windgst_kmh']  ){
                $indice_generale = $indice_generale + 1;
            }elseif(50 <= $row['base']['windgst_kmh']){
                $indice_generale = 0;
            }
        }

            $repository = $this->getDoctrine()->getRepository(IndiceSkiabilite::class);
            $row_ind_base  = $repository->findOneBy(['meteo_commune' => $id_meteo, 'PalierRef' => $base_piste]);
            $repository = $this->getDoctrine()->getRepository(IndiceSkiabilite::class);
            $row_ind_milieu  = $repository->findOneBy(['meteo_commune' => $id_meteo, 'PalierRef' => $milieu_piste]);
            $repository = $this->getDoctrine()->getRepository(IndiceSkiabilite::class);
            $row_ind_haut  = $repository->findOneBy(['meteo_commune' => $id_meteo, 'PalierRef' => $haut_piste]);
            $repository = $this->getDoctrine()->getRepository(IndiceSkiabilite::class);
            $row_ind_base_milieu  = $repository->findOneBy(['meteo_commune' => $id_meteo, 'PalierRef' => $bas_milieu_piste]);
            $repository = $this->getDoctrine()->getRepository(IndiceSkiabilite::class);
            $row_ind_milieu_haut  = $repository->findOneBy(['meteo_commune' => $id_meteo, 'PalierRef' => $milieu_haut_piste]);
            $repository = $this->getDoctrine()->getRepository(IndiceSkiabilite::class);
            $row_ind_base_milieu_haut = $repository->findOneBy(['meteo_commune' => $id_meteo, 'PalierRef' => $bas_milieu_haut_piste]);


        $ind_bas  = $this->verifierExistanceIndice($row_ind_base, $type = "indice");
        $ind_bas->setMeteoCommune($id_meteo);
        $ind_bas->setPalierRef($base_piste);
        $ind_bas->setIndiceSkiabilite($indice_generale+$indice_bas);

        $ind_milieu = $this->verifierExistanceIndice($row_ind_milieu, $type = "indice");
        $ind_milieu->setMeteoCommune($id_meteo);
        $ind_milieu->setPalierRef($milieu_piste);
        $ind_milieu->setIndiceSkiabilite($indice_generale+$indice_milieu);

        $ind_haut = $this->verifierExistanceIndice($row_ind_haut, $type = "indice");
        $ind_haut->setMeteoCommune($id_meteo);
        $ind_haut->setPalierRef($haut_piste);
        $ind_haut->setIndiceSkiabilite($indice_generale+$indice_haut);

        $ind_bas_milieu = $this->verifierExistanceIndice($row_ind_base_milieu, $type = "indice");
        $ind_bas_milieu->setMeteoCommune($id_meteo);
        $ind_bas_milieu->setPalierRef($bas_milieu_piste);
        $ind_bas_milieu->setIndiceSkiabilite($indice_generale+($indice_bas+$indice_milieu)/2);

        $ind_milieu_haut = $this->verifierExistanceIndice($row_ind_milieu_haut, $type = "indice");
        $ind_milieu_haut->setMeteoCommune($id_meteo);
        $ind_milieu_haut->setPalierRef($milieu_haut_piste);
        $ind_milieu_haut->setIndiceSkiabilite($indice_generale+($indice_haut+$indice_milieu)/2);

        $ind_bas_milieu_haut = $this->verifierExistanceIndice($row_ind_base_milieu_haut, $type = "indice");
        $ind_bas_milieu_haut->setMeteoCommune($id_meteo);
        $ind_bas_milieu_haut->setPalierRef($bas_milieu_haut_piste);
        $ind_bas_milieu_haut->setIndiceSkiabilite($indice_generale+($indice_bas+$indice_haut+$indice_milieu)/3);


        $em->persist($ind_bas);
        $em->persist($ind_bas_milieu);
        $em->persist($ind_milieu);
        $em->persist($ind_milieu_haut);
        $em->persist($ind_haut);
        $em->persist($ind_bas_milieu_haut);
    }

}
